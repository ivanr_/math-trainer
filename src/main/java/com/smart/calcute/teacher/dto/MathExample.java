package com.smart.calcute.teacher.dto;

public class MathExample {
    private final int firstNumber;
    private final int secondNumber;
    private final int result;
    private final String mathSign = "*";

    public MathExample(int firstNumber, int secondNumber, int result) {
        this.firstNumber = firstNumber;
        this.secondNumber = secondNumber;
        this.result = result;
    }

    public int getFirstNumber() {
        return firstNumber;
    }

    public int getSecondNumber() {
        return secondNumber;
    }

    public int getResult() {
        return result;
    }

    public String getMathSign() {
        return mathSign;
    }
}
