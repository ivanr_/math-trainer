package com.smart.calcute.teacher.service;

import com.smart.calcute.teacher.dto.MathExample;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;
import java.util.function.Predicate;

@Service
public class MathExampleGeneratorImpl implements MathExampleGenerator {

    public static final int MAX_CAPACITY = 10;
    private final Random random = new Random();
    private final Queue<MathExample> lastExamples;

    public MathExampleGeneratorImpl() {
        this.lastExamples = new LinkedList<>();
        this.lastExamples.add(new MathExample(0, 1, 0));
    }

    @Override
    public MathExample getNext() {
        MathExample newMathExample = null;
        boolean isValidExample = false;
        while (!isValidExample) {
            var currentExample = generateNew();
            isValidExample = lastExamples.stream()
                    .noneMatch(notSimpleOrDuplicatedExample(currentExample));
            newMathExample = currentExample;
        }
        if (lastExamples.size() >= MAX_CAPACITY) {
            lastExamples.remove();
        }
        lastExamples.add(newMathExample);
        return newMathExample;
    }

    private Predicate<? super MathExample> notSimpleOrDuplicatedExample(MathExample currentExample) {
        return mathExampleFromQueue ->
                !isCurrentZerosOrOnesExample(currentExample)
                        & !isDuplicatedExample(mathExampleFromQueue, currentExample)
                        & !isReversedExample(mathExampleFromQueue, currentExample);
    }

    private boolean isReversedExample(MathExample mathExampleFromQueue, MathExample currentExample) {
        return mathExampleFromQueue.getFirstNumber() == currentExample.getSecondNumber()
                & mathExampleFromQueue.getSecondNumber() == currentExample.getFirstNumber();
    }

    private boolean isDuplicatedExample(MathExample mathExampleFromQueue, MathExample currentExample) {
        return mathExampleFromQueue.getFirstNumber() == currentExample.getFirstNumber()
                & mathExampleFromQueue.getSecondNumber() == currentExample.getSecondNumber();
    }

    private boolean isCurrentZerosOrOnesExample(MathExample currentExample) {
        return currentExample.getFirstNumber() != 0
                & currentExample.getSecondNumber() != 0
                & currentExample.getFirstNumber() != 1
                & currentExample.getSecondNumber() != 1;
    }

    private MathExample generateNew() {
        int firstNumber = random.nextInt(10);
        int secondNumber = random.nextInt(10);
        int result = firstNumber * secondNumber;
        return new MathExample(firstNumber, secondNumber, result);
    }
}
