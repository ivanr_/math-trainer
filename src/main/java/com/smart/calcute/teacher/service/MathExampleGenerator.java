package com.smart.calcute.teacher.service;

import com.smart.calcute.teacher.dto.MathExample;

public interface MathExampleGenerator {
    MathExample getNext();
}
