package com.smart.calcute.teacher.controller;

import com.smart.calcute.teacher.dto.MathExample;
import com.smart.calcute.teacher.service.MathExampleGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MathController {

    @Autowired
    private MathExampleGenerator generator;

    @GetMapping("/math/new")
    public MathExample getNew() {
        return generator.getNext();
    }
}
