var input = document.getElementById("result-input");
var response = document.getElementById('response');
var attemptsElement = document.getElementById('attempts');
var actualResult = document.getElementById("actual-result-hidden");
var correctAnswersLabelElement = document.getElementById("correct-answers-label");
var incorrectAnswersLabelElement = document.getElementById("incorrect-answers-label");
var verifyButtonElement = document.getElementById("verify-result-button");

var correctAnswers = 0;
var incorrectAnswers = 0;
window.resizeTo(350, 500);

function addNumber(inputNumber){
    input.value += inputNumber;
}

function removeLastFromInput(){
    input.value = input.value.substring(0, input.value.length - 1);
}
correctAnswersLabelElement.innerHTML = correctAnswers;
incorrectAnswersLabelElement.innerHTML = incorrectAnswers;
loadMathExample();

function loadMathExample() {
    let xhr = new XMLHttpRequest();
    xhr.responseType = 'json';
    xhr.open('GET', '/math/new');
    xhr.send();
    xhr.onload = function() {
      if (xhr.status != 200) { // анализируем HTTP-статус ответа, если статус не 200, то произошла ошибка
        alert(`Ошибка ${xhr.status}: ${xhr.statusText}`); // Например, 404: Not Found
      } else {
        document.getElementById('first-number').innerHTML = xhr.response.firstNumber;
        document.getElementById('second-number').innerHTML = xhr.response.secondNumber;
        document.getElementById('actual-result-hidden').innerHTML = xhr.response.result;
        attemptsElement.innerHTML = 0;
        verifyButtonElement.classList.remove("fail-result-color")
        verifyButtonElement.classList.remove("success-result-color")
        verifyButtonElement.classList.add("init-result-color")
        startTimer();
      }
    };
}

// Execute a function when the user releases a key on the keyboard
input.addEventListener("keyup", function(event) {
  // Number 13 is the "Enter" key on the keyboard
  if (event.keyCode === 13) {
    // Cancel the default action, if needed
    event.preventDefault();
    // Trigger the button element with a click
    input.click();

    verifyResult();
    return;
  }
  if (event.keyCode < 48 || event.keyCode > 57) {
    input.value = input.value.replace(/[^0-9]/g,'');
  }
});

function verifyResult() {
    if (input.value === actualResult.innerHTML) {
        correctAnswersLabelElement.innerHTML = ++correctAnswers;
        verifyButtonElement.classList.remove("fail-result-color")
        verifyButtonElement.classList.remove("init-result-color")
        verifyButtonElement.classList.add("success-result-color")
        setTimeout(() => {
            loadMathExample();
            input.value = "";
            }, 2000);
        stopTimer();
    } else {
        incorrectAnswersLabelElement.innerHTML = ++incorrectAnswers;
        verifyButtonElement.classList.remove("init-result-color")
        verifyButtonElement.classList.add("fail-result-color")
    }
    attemptsElement.innerHTML = parseInt(attemptsElement.innerHTML) + 1;
}

function changeValue() {
  document.getElementById("timer").innerHTML = ++value;
}

var timerInterval = null;
function startTimer() {
  stopTimer(); // stoping the previous counting (if any)
  value = 0;
  timerInterval = setInterval(changeValue, 1000);
}
var stopTimer = function() {
  clearInterval(timerInterval);
}